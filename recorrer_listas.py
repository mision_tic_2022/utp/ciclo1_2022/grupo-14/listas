lista = ['Juan', 'María', 'Alejandra', 'Pedro']

for n in lista:
    print(n)


""" range -> itera en un rango en específico
range(0, 10) -> el primer elemento lo incluye, mientras el último lo excluye
range(0, 100, 2) -> comienza en cero avanzando de 2 en 2 hasta antes de 100
 """
for x in range(10, 100, 2):
    print(x)
