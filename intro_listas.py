listaVacia = []
listaVacia = list()

listaElementos = ['Hola mundo', 10, 3.1416, True]
#Obtener elementos
mensaje = listaElementos[0]
entero = listaElementos[1]
flotante = listaElementos[2]
booleano = listaElementos[3]

print(listaElementos)

#Actualizar elementos
listaElementos[1] = 50
print(listaElementos)

#Añadir elementos
print('----------AÑADIR ELEMENTOS--------')
listaElementos.append(20)
print(listaElementos)

print('----------ELIMINAR ÚLTIMO ELEMENTO---------')
listaElementos.pop()
print(listaElementos)

print('----------ELIMINAR ELEMENTOS POR INDICE---------')
listaElementos.pop(2)
print(listaElementos)